Vvveb.ComponentsGroup['Componentes'] = ["widgets/image-list","widgets/testimonial","widgets/features","widgets/cta"];

Vvveb.Components.extend("_base", "widgets/image-list", {
    nodes: ["ul"],
    name: "Imagen List",
    html: '<ul class="list-group" id="gallery-fff"> </ul>',
    image: "icons/image.svg",
    properties: [{
        name: "Class",
        key: "class",
        htmlAttr: "class",
        inputtype: TextInput
    }, 
    {
        name: "Styles",
        key: "style",
        htmlAttr: "style",
        inputtype: TextInput
    }],
    afterDrop: function (node){
        console.log('drop', node)
        $.ajax({
            url: "https://uifaces.co/api?limit=5",
            type: "GET",
            beforeSend: function(xhr){xhr.setRequestHeader('X-API-KEY', ' 043fb63e0176e4b2a9e6ee2c560f8a');},
            success: function(result) {
                console.log(result)
                for (let index = 0; index < result.length; index++) {
                    const element = result[index];
                    console.log(element.photo);
                    node.append(`<li class="list-group-item"><img src=${element.photo} width="100px"></li>`)
                }
             }
         });
         return node
    }
});

Vvveb.Components.extend("_base",'widgets/testimonial', {
    nodes: ["div"],
    name: "Testimonial",
    html: `<div class="row p-3 text-center flex-column">
                <q class="d-block h2 font-weight-bold">A vaina buena</q>
                <small class="d-block text-secondary font-italic d-block">Jhon Salchichon</small>
            </div>`,


    image: "icons/cart.svg",
	properties: [{
        name: "Id",
        key: "id",
        htmlAttr: "id",
        inputtype: TextInput
    }, {
        name: "Class",
        key: "class",
        htmlAttr: "class",
        inputtype: TextInput
    }],
   
});



Vvveb.Components.extend("_base",'widgets/features', {
    nodes: ["div", "img"],
    name: "Features",
    html: `<div class="row testimonial-list p-3">
                <div class="col-6 d-flex flex-wrap">
                    <h3 class="font-weight-bold">Observa lo que tenemos para ofrecerte</h3>
                    <ul class="pt-3">
                        <li>Es bueno</li>
                        <li>Esta en la playstore</li>
                        <li>Esta en la Appstore</li>
                    </ul>
                </div>
                <div class="col-6">
                    <img src="${ Vvveb.baseUrl}icons/image.svg" class="img-fluid">
                </div>
            </div>`,

    image: "icons/cart.svg",
	properties: [{
        name: "Image",
        key: "src",
        htmlAttr: "src",
        inputtype: ImageInput
    }],
   
});

Vvveb.Components.extend("_base",'widgets/cta', {
    nodes: ["a"],
    name: "CTA",
    html: `<div class="row flex-column align-content-center">
                <h4 class="font-weight-bold">Pruebalo ahora</h4>
                <a href="" class="mt-2 align-self-stretch btn-primary btn">Probar</a>
            </div>`,

    image: "icons/cart.svg",
	properties: [{
        name: "Enlace",
        key: "href",
        htmlAttr: "href",
        inputtype: LinkInput
    }],
   
});

